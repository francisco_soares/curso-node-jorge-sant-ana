use portal_noticias;

-- Crio a tabela
create table noticias(
  id_noticias int NOT NULL PRIMARY KEY AUTO_INCREMENT,
  titulo VARCHAR(100),
  noticias TEXT,
  data_criacao TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
-- Mostro as tabelas dentro do branco portal_noticias
show tables;
-- seleciono a tabela em especifico
select
  *
from
  noticias;
-- Insiro registro
insert into
  noticias(titulo, noticias)
VALUES
  (
    'Outro titulo da noticia',
    'Lorem Ipsum has been the industry s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.'
  );

