var express = require('express') // A variavel [express] recebe o módulo express
var consign = require('consign') // Implemento a instancia do Consign
var bodyParser = require('body-parser') // Implemento o body-parser
var expressValidator = require('express-validator') // Implemento o express validator

// Recebo a execução da função express() na váriavel app
var app = express()

/**
 * Informar para o Express que o engine(moto) de view(visao) do Express passou a ser o modulo [ejs].
 * Dessa forma, os arquivos de visualização não serão mais o [.html] e sim [.ejs]
 */
app.set('view engine', 'ejs')

/**
 * Aqui defino a pasta das views que serão renderizadas.
 * Lembrando que o [./] não está pegando do mesmo nivel desse arquivo e sim do arquivo [app.js] que está na raiz do projeto.
 */
app.set('views', "./app/views")


// INÍCIO [MIDDLEWARE]

// O express.static() acessa os arquivos estáticos e colocar no mesmo nivel da aplicação, dessa forma, podemos acessar uma imagem / css e etc da forma imagem/<nome-arquivo.jpg> ou style/css.css ou js/jquery.js. Sem precisar passar os ../../
app.use(express.static("./app/public")) 

// O body-parser é um middleware, dessa forma, o mesmo deve ser inserido antes do [consign].
// Assim, essa instrução permite pegar os valores do formulário no formato jSON.
app.use(bodyParser.urlencoded({extended: true}))

// Usando o [express validator] para fazer validações, como: notEmpty(), isAlpha(), isInt(), isEmail() e len(4, 8)
app.use(expressValidator())

// FIM [MIDDLEWARE]

// Com o consign é feito um escaneamento dentro da pasta que você especificar.
consign()
    .include('app/routes') // Incluo os arquivos que estiverem dentro da pasta routes para a variavel app.
    .then('config/dbConnection.js') // Incluo esse arquivo dentro da variavel app.
    .then('app/models') // inclui todos os models dentro da variavel app.
    .then('app/controllers') // Inclui todos os controllers dentro da variavel app.
    .into(app)

/**
 * Retorno o módulo app do express
 */
module.exports = app