// Importo o módulo MySQL para aplicação. Esse módulo MySQL foi instalado via NPM.
var mysql = require('mysql')

// Usando o método rapper.
// Onde crio uma função e defino a mesma para a variável [connMySQL], dessa forma não é executado a conexão assim que o servidor é ativado.
var connMySQL = function () {
    // Estabelecendo a conexão com o bando de dados.
    // Ao usar o método createConnection, você deve passar os parametro no formato JSON[ { chave : valor }]
    return mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: '',
        database: 'portal_noticias'
    })
}

/**
 * Crio o módulo e exporto o mesmo para ser pego nas routes.
 * Um exemplo está no arquivo noticias.js dentro da pasta app/routes/
 * 
 * @returns string
 */
module.exports = function () {
    // Usando o método rapper, evita que seja feito a conexao a cada solicitação feita.
    // Dessa forma, é retornado apenas a função se ser executada.
    // Dentro do arquivo server.js que está na pasta /config/ esse arquivo dbConnection.js é incluido dentro de uma variavel app.
    return connMySQL
}