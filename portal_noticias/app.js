// Trasendo o módulo app no express que está no arquivo config/server.js
var app = require('./config/server')

// var rotaNoticias = require("./app/routes/noticias")(app)
// rotaNoticias(app)

// var rotaHome = require("./app/routes/home")(app)
// rotaHome(app)

// var rotaFomInclusaoNoticia = require("./app/routes/formulario_inclusao_noticia")(app)
// rotaFomInclusaoNoticia(app)

/** [Aula 13]
 * O método listen() recebe a porta de escuta, seria 3000 e recebe um callback/execução de uma função.
 */
app.listen(3000, function () {
    console.log("Servidor ON")
})