/**
 * Implementando a estrutura de CLASSES
 * Para entender melhor acesse os links abaixo:
 * Aula 41: 
 * Aula 42: https://www.udemy.com/course/curso-completo-do-desenvolvedor-nodejs/learn/lecture/5591414#overview
 */
function NoticiasDAO(connection) {
    this._connection = connection
}

// Executando a consulta 
// A função query(<sql>, <func callback>) | SQL: é a consulta | CALLBACK: é o que vai ser feito após a consulta ser realizada.
NoticiasDAO.prototype.getNoticias = function (callback) {
    this._connection.query('select * from noticias order by data_criacao desc', callback)
}

// Executando a consulta 
// A função query(<sql>, <func callback>) | SQL: é a consulta | CALLBACK: é o que vai ser feito após a consulta ser realizada.
NoticiasDAO.prototype.getNoticia = function (id_noticias, callback) {
    this._connection.query('select * from noticias where id_noticias = ' + id_noticias.id_noticia, callback)
}

NoticiasDAO.prototype.salvarNoticia = function (noticia, callback) {

    // Insiro a noticia
    // Aqui é a mesma questão do bindValue do PHP
    // O insert do módulo instalado do MySQL entende que o segundo parametro [noticia] é um jSON, dessa forma ele transforma tudo em STRING e pega tudo que está depois no ? e inseri.
    this._connection.query('insert into noticias set ?', noticia, callback)
}

// Método/Função que retorna as 5 últimas noticias.
NoticiasDAO.prototype.get5UltimasNoticias = function(callback){
    this._connection.query('SELECT * FROM noticias ORDER BY data_criacao DESC LIMIT 5', callback)
}

module.exports = function () {
    return NoticiasDAO
}