module.exports.noticias = function (application, req, res) {
    // Atribuo a execução da função dbConnection
    // Essa função dbConnection() está vindo do arquivo server.js, na linha onde contém esse código .then('config/dbConnection.js').
    var connection = application.config.dbConnection()

    // Pego esse módulo pelo autoload do consig definido no arquivo server.js, mais especifico no código .then('app/models').
    var noticiasModel = new application.app.models.NoticiasDAO(connection);

    // Pego a função getNoticias do arquivo NoticiasDAO.js e passo por parametro a connection e função de callback.
    // A função de callback recebe dois parametros (error, result)
    // erro: se der um erro, conseguimos recuperar o erro e tratar.
    // result: recebe o retorno da query.
    noticiasModel.getNoticias(function (error, result) {

        // Dentro da view noticias, será passado a variavel/array [noticias] que recebe o [result]             
        res.render("noticias/noticias", {
            noticias: result
        })
    })
}

module.exports.noticia = function (application, req, res) {
    // Atribuo a execução da função dbConnection
    // Essa função dbConnection() está vindo do arquivo server.js, na linha onde contém esse código .then('config/dbConnection.js').
    var connection = application.config.dbConnection()

    // Pego esse módulo pelo autoload do consig definido no arquivo server.js, mais especifico no código .then('app/models').
    var noticiasModel = new application.app.models.NoticiasDAO(connection)

    // Recupera o valor passado por parametro passado via URL em formato JSON
    // Para entender, de um console.log(req.query)
    // noticia?id_noticia=<num-id_noticias>
    var id_noticias = req.query

    // Pego a função getNoticias do arquivo NoticiasDAO.js e passo por parametro a connection e função de callback.
    // A função de callback recebe dois parametros (error, result)
    // erro: se der um erro, conseguimos recuperar o erro e tratar.
    // result: recebe o retorno da query.
    noticiasModel.getNoticia(id_noticias, function (error, result) {
        // res.send(result)
        // Dentro da view noticias, será passado a variavel/array [noticias] que recebe o [result]             
        res.render("noticias/noticia", {
            noticia: result
        })
    })
}