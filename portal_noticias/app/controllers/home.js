module.exports.index = function (application, req, res) {

    // Pego a conexão com o bando dedados
    var connection = application.config.dbConnection()

    // Pego o model NoticiasDAO()
    var noticiasModel = new application.app.models.NoticiasDAO(connection)

    // Recuperar via Model as 5 últimas noticias
    noticiasModel.get5UltimasNoticias(function (error, result) {
        res.render("home/index", {
            noticias: result
        })
    })


}