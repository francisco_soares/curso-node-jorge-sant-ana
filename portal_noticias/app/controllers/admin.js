module.exports.formulario_inclusao_noticia = function (application, req, res) {
    res.render("admin/form_add_noticia", {
        validacao: {},
        noticia: {}
    })
}

module.exports.noticias_salvar = function (application, req, res) {
    // O reg.body pega todos os dados via post passado pelo formulário.
    var noticia = req.body

    // Validando os campos obrigatórios com express-validator
    // O método assert(<1º-parametro>, <2º-parametro>) recebe dois parametros.
    // 1º Parametro: o campo NAME do INPUT
    // 2º Parametro: A mensagem, ex: O campo título é obrigatório! 
    req.assert('titulo', 'O campo TÍTULO é obrigatório!').notEmpty()
    req.assert('resumo', 'O campo RESUMO é obrigatório!').notEmpty()
    req.assert('resumo', 'O campo RESUMO deve conter entre 10 e 100 caracteres').len(10, 100)
    req.assert('autor', 'O campo AUTOR é obrigatório').notEmpty()
    req.assert('data_noticia', 'O campo DATA DOS FATOS é obrigatório').notEmpty().isDate({
        format: 'YYY-MM-DD'
    })
    req.assert('noticias', 'O campo NOTICIAS é obrigatório').notEmpty()

    var erros = req.validationErrors()

    if (erros) {
        res.render("admin/form_add_noticia", {
            validacao: erros,
            noticia: noticia
        })
        return
    }
    // Atribuo/recupero a conexão
    // Essa função dbConnection() está vindo do arquivo server.js, na linha onde contém esse código .then('config/dbConnection.js').
    var connection = application.config.dbConnection()

    // Pego esse módulo pelo autoload do consig definido no arquivo server.js, mais especifico no código .then('app/models').
    var noticiasModel = new application.app.models.NoticiasDAO(connection);

    // Pego a função getNoticias do arquivo NoticiasDAO.js e passo por parametro os dados enviado via formulário na variavel [noticia], junto a função de callback.
    // A função de callback recebe dois parametros (error, result)
    // erro: se der um erro, conseguimos recuperar o erro e tratar.
    // result: recebe o retorno da query.
    noticiasModel.salvarNoticia(noticia, function (error, result) {

        // Redireciono o usuário depois do envio do formulário.
        res.redirect('/noticias')
    })
}