/**
 * Função de callback que retorna as views
 * 
 * O parametro [application] passado nessa função de callback, está vindo no arquivo [server.js], onde esse módulo é chamado.
 * 
 * Dentro do arquivo [server.js], o código .include('app/routes') expecifica que todas as rotas são escaneada dentro do consign. No final dessa instrução tem o .into(app), é aqui que acontece a passagem de parametros.
 * Desas forma que é pego o application nesse módulo.
 * 
 * @param {string} application 
 */
module.exports = function (application) {
    /**
     * [Renderizo a página /formulario_inclusao_noticia]
     * Lembrando que aqui está sendo passado via GET
     */
    application.get('/formulario_inclusao_noticia', function (req, res) {
        application.app.controllers.admin.formulario_inclusao_noticia(application, req, res)
    })

    /**
     * [Renderizo a página /salvar]
     * Lembrando que aqui está sendo passado via POST
     */
    application.post('/noticias/salvar', function (req, res) {
        application.app.controllers.admin.noticias_salvar(application, req, res)
    })
}