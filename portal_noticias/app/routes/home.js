/**
 * Função de callback que retorna a view index
 * 
 * O parametro [application] passado nessa função de callback, está vindo no arquivo [server.js], onde esse módulo é chamado.
 * 
 * Dentro do arquivo [server.js], o código .include('app/routes') expecifica que todas as rotas são escaneada dentro do consign. No final dessa instrução tem o .into(app), é aqui que acontece a passagem de parametros.
 * Desas forma que é pego o application nesse módulo.
 * @param {string} application 
 */
module.exports = function (application) {
    /** 
     * Pego a pagina / com o get().
     * Passo uma função de callback com parametros de req/requisição e res/resposta.
     * Com o render() eu informo a pasta que vou pegar as views.
     * No caso abaixo será a pasta [home] e o arquivo [index.ejs]. O motor do ejs já sabe que a extensão da view e .ejs então eu posso ocultar e deixar apenas [index]
     * 
     * [RENDERIZO A VIEW INDEX]
     */
     application.get('/', function (req, res) {
        application.app.controllers.home.index(application, req, res)
    })
}