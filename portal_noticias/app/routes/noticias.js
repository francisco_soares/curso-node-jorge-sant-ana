/**
 * Função de callback que retorna a view noticias
 * 
 * O parametro [application] passado nessa função de callback, está vindo no arquivo [server.js], onde esse módulo é chamado.
 * 
 * Dentro do arquivo [server.js], o código .include('app/routes') expecifica que todas as rotas são escaneada dentro do consign. No final dessa instrução tem o .into(app), é aqui que acontece a passagem de parametros.
 * Desas forma que é pego o application nesse módulo.
 * @param {string} application 
 */
module.exports = function (application) {

    // [Renderizo a view/noticias]    
    application.get('/noticias', function (req, res) {
        application.app.controllers.noticias.noticias(application, req, res)
    })

    // [Renderizo a view/pagina de /noticia]    
    application.get('/noticia', function (req, res) {
        application.app.controllers.noticias.noticia(application, req, res)
    })

}