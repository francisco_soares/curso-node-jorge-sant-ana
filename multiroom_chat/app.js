/**
 * O arquivo app.js é um entrypoint da aplicação.
 * A partir dele é configurado para escutar as requisições da aplicação.
 * 
 */

/* Importar as configurações do servidor */
const {
    set
} = require('./config/server')
var app = require('./config/server')

/**
 * Parametrizar a porta de escuta
 * Sintax: app.listen(<numero-porta>, <func-callback>)
 */
var server = app.listen(80, function () {
    console.log('Servidor ON')
})

// Preparando o servidor para responder a websocket
// Aqui Importo o socket.io e informo a posta onde ele vai escutar. Também é a mesma porta do http
var io = require('socket.io').listen(server)

/**
 * A função set também é usado para criar variáveis globais.
 * Dessa forma, estou criando a variável GLOBAL IO com o valor IO acima.
 */
app.set('io', io)

/**
 * Criar a conexão por websocket
 * 
 * O connection dentro do ON() é pego no momento de uma tentativa de conexão.
 * Isso vindo da function io() la no arquivo multiroom_chat\app\views\chat.ejs
 * 
 * O parametro socket da função vem pelo evento connection do on()
 */
io.on('connection', function (socket) {
    console.log('Usuário conectou')

    /**
     * Quando você desconecta do chat, a função abaixo e disparada.
     * No arquivo multiroom_chat\app\views\chat.ejs, quando você sai da página, o mesmo não existe mais a instancia do socket.io definido na parte de baixo do script da página multiroom_chat\app\views\chat.ejs.
     * Dessa forma o disconnect sabe quando você saiu da aplicação.
     */
    socket.on('disconnect', function () {
        console.log('Usuário desconectou')
    })

    /**
     * Escuto a mensagem passada no emit() do arquivo app\views\chat.ejs, aqui: socket.emit('msgParaServidor'
     * 
     * E envio uma nova requisição para o socket.on('msgParaCliente') no arquivo app\views\chat.ejs
     */
    socket.on('msgParaServidor', function (data) {

        // Esse envio aparece apenas para quem enviou a mensagem
        socket.emit('msgParaCliente', {
            apelido: data.apelido,
            mensagem: data.mensagem
        })

        // Esse broadcast envia a mensagem que eu enviei para os outros usuário e não para quem enviou.
        socket.broadcast.emit('msgParaCliente', {
            apelido: data.apelido,
            mensagem: data.mensagem
        })


        /**
         * Nessa condição, eu verifico se o apelido_atualizado_nos_clientes == 0, caso seja verdadeiro, entra na condição.
         * Isso é usado para mostrar a quantidade de participantes no chat.
         */
        if (parseInt(data.apelido_atualizado_nos_clientes) == 0) {
            /**
             * Atualiza a relação de participantes no chat.
             */
            // Esse envio aparece apenas para quem enviou a mensagem
            socket.emit('participantesParaCliente', {
                apelido: data.apelido
            })

            // Esse broadcast atualiza para todos os usuários que estão no chat.
            socket.broadcast.emit('participantesParaCliente', {
                apelido: data.apelido
            })
        }

    })
})