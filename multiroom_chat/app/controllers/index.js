/**
 * Função responsavel por renderizar a entrega de uma solicitação
 * 
 * Essa função será exportada
 * 
 * @param {string} application 
 * @param {string} req 
 * @param {string} res 
 */
module.exports.home = function (application, req, res) {
    // Dentro do render, estou passando o parametro [validacao: {}] = vazio. Pois dentro da index.ejs, ela variavel é pega para fazer uma condição.
    // E de onde está vindo essa variavel?
    // Está vindo do arquivo app\controllers\chat.js
    res.render('index', {validacao: {}})
}