module.exports.iniciaChat = function (application, req, res) {

    // Pegando os valores passado pelo formulário
    var dadosForm = req.body;

    // Faço as validações usando o módulo body-parser com o método assert()
    req.assert('apelido', 'Nome ou Apelido é obrigatório').notEmpty()
    req.assert('apelido', 'Nome ou Apelido deve ter entre 3 e 15 caracteres').len(3, 15)

    // Função que retorna os erros
    var erros = req.validationErrors()

    if (erros) {
        res.render("index", {
            validacao: erros
        })
        return
    }

    /**
     * Faz pedido para executar alguma ação.
     * 
     * O pedido será escutado na função ON() no arquivo app\views\chat.ejs 
     * 
     * O comando  application.get('io') está vindo do arquivo \app.js, mais específico no comando app.set('io', io).
     * O mesmo está criando uma variável global.  Lembrando com o APP do \app.js é o mesmo application aqui passado por parametro module.exports.iniciaChat = function (application, req, res)
     **/
    application.get('io').emit('msgParaCliente', {
        apelido: dadosForm.apelido,
        mensagem: 'acabou de entrar no Chat.'
    })

    res.render('chat', {
        dadosForm: dadosForm
    })
}