# **Curso Node - Jorge Sant Ana**

Curso de 2016.<br>
Instrutor: Jorge Sant Ana<br><br>


## **Aula 1 - Introdução ao NodeJS**
**O que é NodeJS**
- Interpretador de códigos JavaScript
- Baseado no V8 do Google Chrome. Ele é um interpretador JavaScript
- Ele é escrito em C++
- O Node é instalado no computador
- Podemos criar tanto script para front-end quando back-end.

Link da aula: https://www.udemy.com/course/curso-completo-do-desenvolvedor-nodejs/learn/lecture/5957260#overview
<br><br>

## **Aula 3 - Entendendo o conceito client-server e a dinâmica de aplicação**

**Client-Server**
- Server: é uma aplicação com diversos serviços que podem ser requisitados por outras aplicações (client).
- Client: é uma aplicação que pode requisitar uma ação a uma aplicação servidora.
- Para obter comunicação entre eles, é preciso de uma padrão de protocolo.
- Client: Temos os Browsers (Navegadores)
- Server: Temos os Servidor Web (HTTP)
- Browser: Envia uma requisição ao servidor Web. Ex: Browser ----->(request) Servidor Web.
- Servidor Web: Retorna a uma resposta para o browser tratar. Ex: Browser <-----(Response) Servidor | 
<br><br>

## **Aula 10 - Visão geral do NPM, Express, EJS e Nodemon**

**EXPRESS** - Um framework NodeJS para aplicações WEB <br>
**EJS** - Uma linguagem de modelagem para criação de páginas HTML utilizando JavaScript<br>
**NODEMON** - Um utilitário que reinicia automaticamente o servidor NodeJS quando houver qualquer alteração em nossos scripts<br>
**NPM** - Um gereciador de pacotes JavaScript<br><br>


## **Aula 11 - Iniciando o NPM em nosso projeto**
COMANDOS E DESCRIÇÕES:<br>

```
npm -v
```
Verifica a versão do NPM
_______________________

```
npm init
```

**ATENÇÃO**: O comando **npm init** deve ser utilizado na pasta do **PROJETO** que você vai iniciar.<br><br>

Um comando de passo-a-passo. Ele fará uma séria de perguntas, irei listar todas abaixo.<br>
**name:** - Nome do projeto. Você pode inserir o nome do seu projeto, no meu caso será **portal_noticias**.<br>
**version** - Deixo a versão que ele sugere<br>
**description** - Coloco uma descrição sobre o projeto<br>
**entry point** - É o arquivo que será o ponto de entrada que conterá a lógica principal do módulo. Como estamos criando um sistema web, não é preciso criar. Caso queira criar um módulo e disponibilizar, será preciso criar.<br>
**teste command** - Não vamos utilizar, apenas de um enter<br>
**git repository** - Seria o link do repositório no git, não precisa usar.<br>
**keywords** - Não precisa preencher, só dar um enter<br>
**author** - Colocar o seu nome<br>
**license** - Deixe a sugerida, apenas de enter<br>
**Pergunta** - Apenas de YES e enter<br><br>

## **Aula 12 - NPM - Instalando o Express**
COMANDOS E DESCRIÇÕES:<br>

```
npm install express@4.15.3 -save
```
**ATENÇÃO**: O comando **npm install express@4.15.3 -save** deve ser utilizado na pasta do **PROJETO** que você vai iniciar.<br><br>

- O comando **npm install express@4.15.3** instala a versão usada no curso.
- O comando **-save**, trás os arquivos de fato para dentro do projeto, será criado um diretório onde será inserido o Express como um módulo da nossa aplicação.<br><br>

## **Aula 14 - NPM - Instalando o EJS**
COMANDOS E DESCRIÇÕES:<br>

```
npm install ejs@2.5.6 --save
```

**ATENÇÃO**: O comando **npm install ejs@2.5.6** deve ser utilizado na pasta do **PROJETO** que você vai iniciar.<br><br>

- Comando para executar arquivos html para o usuário.
- O comando **npm install ejs@2.5.6** instala a versão usada no curso.
- O comando **--save**, trás os arquivos de fato para dentro do projeto.<br><br>


## **Aula 17 - NPM - Instalando e testando o Nodemon**
COMANDOS E DESCRIÇÕES:<br>

```
npm install -g nodemon@1.10.2
```

**ATENÇÃO**: O comando **npm install -g nodemon@1.10.2** deve ser utilizado na pasta do **PROJETO** que você vai iniciar.<br><br>
- O comando **npm install -g nodemon@1.10.2** instala a versão usada no curso.<br><br>

# **Subindo o servidor já com o NODEMON instalado**

Para subir o servidor no projeto **Portal de Noticias**, você precisa acessar a pasta **portal_noticias** e digitar o código.

```
nodemon <nome-arquivo.js>
nodemon app.js

Use o código abaixo se não estiver com o nodemon instaldo no projeto
node <nome-arquivo.js>
node app.js
```

## **Aula 21 - NPM - Instalando o módulo de conexão com MySQL**
COMANDOS E DESCRIÇÕES:<br>

```
npm install mysql@2.13.0 --save
```

**ATENÇÃO**: O comando **npm install mysql@2.13.0** deve ser utilizado na pasta do **PROJETO** que você vai iniciar.<br><br>
- O comando **npm install mysql@2.13.0** instala a versão usada no curso.<br><br>

## **Aula 29 - O que é o Consign**
- Ajuda a incluir automáticamente alguns módulos em nossa aplicação.<br>
- Ele é muito util para incluir as rotas da nossa aplicação.<br><br>


## **Aula 30 - NPM - Instalando o Consign**
COMANDOS E DESCRIÇÕES:<br>

```
npm install consign@0.1.6 --save
```

**ATENÇÃO**: O comando **npm install consign@0.1.6** deve ser utilizado na pasta do **PROJETO** que você vai iniciar.<br><br>
- O comando **npm install consign@0.1.6** instala a versão usada no curso.<br><br>

## **Aula 39 - NPM - Instalando o Body-parser**
COMANDOS E DESCRIÇÕES:<br>

```
npm install body-parse@1.17.2 --save
```

**ATENÇÃO**: O comando **npm install body-parser@1.17.2** deve ser utilizado na pasta do **PROJETO** que você vai iniciar.<br><br>
- O comando **npm install body-parser@1.17.2** instala a versão usada no curso.<br><br>


## **Aula 44 - NPM - Instalando o Express Validator**
COMANDOS E DESCRIÇÕES:<br>

```
npm install --save express-validator@3.2.0 -E
```

**ATENÇÃO**: O comando **npm install express-validator@3.2.0 -E** deve ser utilizado na pasta do **PROJETO** que você vai iniciar.<br><br>
- O comando **npm install express-validator@3.2.0 -E** instala a versão usada no curso.<br><br>

## **Aula 64 4 64 - NPM - Instalando o Socket.IO**
COMANDOS E DESCRIÇÕES:<br>

```
npm install socket.io@2.0.3 --save
```

**ATENÇÃO**: O comando **npm install socket.io@2.0.3** deve ser utilizado na pasta do **PROJETO** que você vai iniciar.<br><br>
- O comando **npm install socket.io@2.0.3 --save** instala a versão usada no curso.<br>
- **IMPORTANTE**: para saber mais sobre o **SOCKET** acessa a aula 64: https://www.udemy.com/course/curso-completo-do-desenvolvedor-nodejs/learn/lecture/5754018#overview <br><br>

## **Aula 64 4 64 - NPM - Instalando o Socket.IO**
COMANDOS E DESCRIÇÕES:<br>

```
npm install mongodb --save
```
**ATENÇÃO**: O comando **npm install mongodb --save** deve ser utilizado na pasta do **PROJETO** que você vai iniciar.<br><br>

## **Aula 93 - NPM - Instalando o Express-Session**
COMANDOS E DESCRIÇÕES:<br>

```
npm install express-session --save
```
**ATENÇÃO**: O comando **npm install express-session --save** deve ser utilizado na pasta do **PROJETO** que você vai iniciar.<br><br>

