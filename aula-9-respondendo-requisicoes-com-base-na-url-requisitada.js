/**
 * 
 */

var http = require('http')

var server = http.createServer(function (req, res) {

    /**
     * No req.url, está pegando do / para frente
     * Ex: localhost:3000/moda
     * Então o req.url pegará o /moda
     */
    var categoria = req.url

    if(categoria == "/tecnologia"){
        res.end("<html><body>Portal de Tecnologia</body></html>")
    } else if(categoria == "/moda"){
        res.end("<html><body>Portal de Moda</body></html>")
    } else if(categoria == "/beleza"){
        res.end("<html><body>Portal de Beleza</body></html>")
    }else{
        res.end("<html><body>Portal de notíciass</body></html>")
    }
    
})

server.listen(3000)

/**
 * No script acima foi implementado o URL que é um dos atributos do req.
 * 
 * Com ele podemos verificar as páginas que estão sendo requisitadas pelo usuários.
 * 
 * LEMBRE:
 * Toda alteração feita, precisa ser re-startado o servidor novamente.
 */