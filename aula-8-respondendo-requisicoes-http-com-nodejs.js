/**
 * O HTTP dentro do require é uma biblioteca Javascript
 * 
 * O REQUIRE é uma propriedade que permite incorporar outros arquivos dentro do arquivos em questão.
 */
var http = require('http')

/**
 * createServer: Cria um servidor
 * Dentro do createServer é passado um parametro que recebe um REQUISIÇÃO(req) e retorna uma RESPOSTA(res)
 */
var server = http.createServer(function(req, res){
    // O servidor sempre recebe uma requisição(req) e retorna uma resposta(res)
    res.end("<html><body>Portal de notícias</body></html>")
})

/**
 * O listen() é utilizado para informar ao servidor em qual porta ele deve escutar
 */
server.listen(3000)

/**
 * TESTE DO SCRIPT ACIMA
 * Para executar os testes, você precisa primeiramente acessar a pasta do script. Nesse caso a pasta é curso-node-jorge-sant-ana
 * 
 * Dentro do Bash, Prompt ou PowerShell você executa o código: node <nome-arquivo.js>
 * 
 * node aula-8-respondendo-requisicoes-http-com-nodejs.js
 * 
 * Ao executar no Bash, Prompt ou PowerShell, o mesmo ficará travado.
 * Você deve fazer uma requisição ao servidor, então acesse o navegador(browser) e digite localhost:3000
 * 
 * 3000 é a porta definida na propriedade listen().
 * 
 * Para derrubar/parar o servidor, apenas aperte CTRL+C
 * 
 * Então, o script acima foi criado um servidor utilizando a biblioteca HTTP e criando o servidor para receber as requisições e retornar as respostas.
 */