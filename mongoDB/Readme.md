## **Aula 73 - Instalando o Bando de Dados MongoDB**

Após instalar o MongoDB, você deve seguir as seguintes orientações abaixo<br><br>

- 1º Passo: Acesse a pasta do MongoDB e execute o arquivo **mongod.exe**. Isso no **Windows**
```
cd c:\Program Files\MongoDB\Server\5.0\bin>mongod.exe
```
- 2º Após subir o **mongod.exe**, você deve subir o **mongo.exe**
```
cd c:\Program Files\MongoDB\Server\5.0\bin>mongo.exe
```
<br>

## **Aula 74 - Exibindo, criando e removendo bancos de dados**
<br>

Comando para mostrar todos os bancos.
```
show dbs;
```
- Dica: você pode ou não usar o (;) depois do comando.<br><br>

Comando para trocar de banco e para criar
```
use curso_mongodb
```
- O comando acima vai procurar o banco **curso_mongodb**, caso encontre, o mesmo é selecionado. Caso não encontre, esse comando vai preparar toda a estrutura para criação do banco

Criando, após o uso do comando **use curso_mongodb**, para iniciar a criação do banco, logo em seguida você pode usar o comando abaixo:
```
db.alunos.save({Nome: "Francisco"})
```
<br>

Comando para Excluir um banco.<br>
Primeiro acesse o banco com o comando **use curso_mongodb**, em seguida digite o comando abaixo:
```
db.dropDatabase();
```


## **Aula 75 - Exibindo, criando e removendo bancos de dados**
<br>

**O que é uma Collection?**<br>
Fazendo uma analogia com os banco de dados relacionais, as collection/coleções seriam as tabelas.<br><br >

Comando para criar uma Collection<br>
```
db.createCollection("alunos")
```

Comando para mostrar as Collection de um banco de dados<br>
```
db.getCollectionNames()
```

Comando para remover a Collection.
```
db.<nome-collection>drop()
db.cursos.drop()
```
<br>

## **Aula 76 - Inserindo documentos**
<br>

- Como comentado, uma Collection é a tabela se formos comparar com um banco relacional.
- Uma collection não obriga ter uma estrutura, então, cada documento pode ter seus atributos e valores e pode não ter relação nenhuma entre esses atributos e valores, de um documento para outro.
- Os documentos, são armazenado na notação JSON.

<br>
Comando para inserir dados em uma collection

```
db.alunos.save(
    {
        nome: 'José', 
        idade: 30, 
        sexo: 'M', 
        cpf: '123.123.123-12', 
        rg: '123.123.123-1', 
        matricula: 'abcd123'
    }
)

OUTRA FORMA:

db.alunos.save( 
    { 
        nome: 'Fernanada', 
        idade: 32, sexo: 'F', 
        matricula: 'hjk456', 
        cursos_interesse: [ 
            {curso: 'Curso Completo de Desenvolver NodeJS'}, 
            {curso: 'Curso Completo de Desenvolvimento WEB - Crie 6 projetos'} 
        ] 
    } 
)
```
<br>

## **Aula 77 - Consultando documentos com operadores de comparação**
<br>

**$eq** = Equals - É igual<br>
**$gt** = Greater Than - É maior que <br>
**$gte** = Greater Than or Equal - É maior ou igual a <br>
**$lt** = Less Than - É menor que <br>
**$lte** = Less Than or equal - É menor ou igual a<br>
**$ne** = Not Equal - É diferente de <br>
<br>

Comando que trás o último documento inserido dentro de uma Collection
```
db.alunos.findOne()
```
<br>

Comando que trás todos os documentos dentro de nossa Collection de uma forma não tão amigável.
```
db.alunos.find()
```
<br>

Comando que trás todos os documentos dentro de nossa Collection de uma forma mais amigável.
```
db.alunos.find().pretty()
```
<br>

Usando o comando **find()** com o operador de comparação **$eq** = Equals
```
db.alunos.find({nome: {$eq: "Francisco"}}).pretty()
```

Pesquisando documento onde a **idade** é menor que(**$lt** - less than) 50
```
db.alunos.find({idade: {$lt: 50}}).pretty()
```

Pesquisando documento onde a **idade** é menor ou igual que(**$lte** - Less Than or equal) 30
```
db.alunos.find({idade: {$lte: 30}}).pretty()
```

Pesquisando documento onde a **idade** é maior que(**$gt** - Greater Than) 30
```
db.alunos.find({idade: {$gt: 30}}).pretty()
```

Pesquisando documento onde a **idade** é maior ou igual a(**$gte** - Greater Than or Equals) 30
```
db.alunos.find({idade: {$gte: 30}}).pretty()
```

Pesquisando documento onde a **sexo** é diferente a(**$ne** - Not Equals) M
```
db.alunos.find({sexo: {$ne: 'M'}}).pretty()
```

## **Aula 78 - Consultando documentos com operadores lógicos**
<br>

Usando o operador lógico **AND**
```
.db.alunos.find(
    {
        sexo: {$eq: 'F'},
        idade: {$gt: 30}
    }
).pretty()
```
<br>

Usando o operador lógico **OR**
```
db.alunos.find({
    $or: [
        {nome: {$eq: 'Francisco'}},
        {nome: {$eq: 'Isa'}}
    ]
}).pretty()
```

## **Aula 79 - Atualizando documentos**
<br>

**update()** - Comando para atualizar registros.<br>
_Sintax:_
```
update(
    {<parametros>}, 
    {$set},
    {mult: false} 
)

{<parametros>} - É os parametros para atualização/ A condição que deve ser atendida.
{$set} - quais campos vao sofrer as alterações
{mult: false} - Por default/padrao ele vem como false

Mais sobre o {mult: false}
Se deixarmos o mult como false ou nao mecionarmos no update(), ele só vai agir no primeiro documento especificado na condição {<parametros>}.

Agora, se deixarmos o {mult: true}, ele vai alterar todos os DOCUMENTOS QUE ATENDEREM A CONDIÇÃO {<parametro>}

CÓDIGO:

db.alunos.update(
    {nome: 'José'},
    {$set: {nome: 'João'}}
)

OUTR FORMA:

db.alunos.update(
    {nome: 'José'},
    {$set: {nome: 'João', idade: 30}}
)

```

**save()** - Outras caracteristicas peculiares.

```
save(
    _id: xxxx
)

O comando SAVE(), além de inserir um novo registro ele meio que "atualiza".
Como seria isso.
Ao passado o _id que exista o comando save() vai pegar o registro contido e vai substituir pelo um novo registro.
TOME CUIDADO.
Porém, se ele não encontrar o documento, será  incluido um novo documento.
```

## **Aula 80 - Removendo documentos**
<br>

**remove()** - Comando para remover documentos.<br>
_Sintax:_

```
remove(
    {critério de exclusão},
    true ou false / 1 ou 0 (justone)
)

{critério de exclusão} - É o que determina a exclusão
true ou false / 1 ou 0 (justone) - Esse parametro pode ser omitido. Caso TRUE/1 - Será removido o 1º documento que ele encontrar estabelecido no critério de exclusão. Caso seja FALSE/0, será excluido todos os documento que entre no critério de exclusão.

CÓDIGO:

db.alunos.remove(
    {nome: 'Maria'} ou {nome: {$eq: 'maria'}}
)

OUTRA FORMA COM CONDICIONAL

db.alunos.remove(
    {idade: {$gt: 30}}
)
```