/* Importando o Banco MongoDB */
var mongo = require('mongodb')

var connMongoDB = function () {

    /** Instanciando a classe de conexão com o MongoDb()
     * 
     * 1º Parametro: String do nome do banco de dados
     * 
     * 2º Parametro: Obj de conexão com o servidor
     *    1º Parametro: String do endereço onde nosso banco está
     *    2º Parametro: Porta de conexão
     *    3º Parametro: Obj com as opções de configuração do servidor
     * 
     * 3º Parametro: Obj com as opções adicionais de configurações.
     */
    var db = new mongo.Db(
        'got',
        new mongo.Server(
            'localhost',
            27017, {}
        ), {}
    )

    // Retorna o OBJ que foi criado para ser usado dentro dos Models.
    return db
}

/* Expostando o módulo do MongoDB para ser pego no Consign */
module.exports = function () {
    return connMongoDB;
}