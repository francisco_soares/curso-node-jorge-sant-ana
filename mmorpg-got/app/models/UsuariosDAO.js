/**
 * O UsuariosDAO está sendo usado como uma especie de "CLASSE"
 */
function UsuariosDAO(connection) {
    /* Executo a conexão (connection()) e atribuo essa conexão a variavel _connection. Assim posso usar em outros módulos. */
    this._connection = connection()
}

/**
 * O inserirUsuario é como se fosse um método da "CLASSE" UsuariosDAO
 * @param {json} usuario 
 */
UsuariosDAO.prototype.inserirUsuario = function (usuario, req, res) {

    /**
     * O método open() é utilizando para manupular os DOCUMENTOS/registro no Mongo
     *  1º Parametro: Erro caso retorne.
     *  2º Parametro: O obj cliente com as tratativas.
     */
    this._connection.open(function (err, mongoclient) {

        /** 
         * O collection recebe dois parametros
         *  1º Parametro: É a collection, que no caso, será usuarios
         *  2º Parametro: Função de callback para manipular uma ação.
         */
        mongoclient.collection("usuarios", function (err, collection) {
            collection.insert(usuario, function (err, records) {
                if (err) {
                    console.log(err)
                } else {
                    // Caso não apresente nenhum erro, jogo para a view sucesso
                    res.render('sucesso', {sucesso: 'sucesso'})
                }
            })

            /* Fecho o método open() após a inserção de um registro. */
            mongoclient.close()
        })
    })
}

/**
 * O autenticar é como se fosse um método da "CLASSE" UsuariosDAO
 * 
 * @param {string} usuario 
 * @param {string} req 
 * @param {string} res 
 */
UsuariosDAO.prototype.autenticar = function (usuario, req, res) {
    /**
     * O método open() é utilizando para manupular os DOCUMENTOS/registro no Mongo
     *  1º Parametro: Erro caso retorne.
     *  2º Parametro: O obj cliente com as tratativas.
     */
    this._connection.open(function (err, mongoclient) {

        /** 
         * O collection recebe dois parametros
         *  1º Parametro: É a collection, que no caso, será usuarios
         *  2º Parametro: Função de callback para manipular uma ação.
         */
        mongoclient.collection("usuarios", function (err, collection) {

            /* Esse é um jeito de fazer, mais robusto */
            /* O toArray retorna o cursor convertido em array que o find() retorna para o usuário. */
            collection.find({
                usuario: {
                    $eq: usuario.usuario
                },
                senha: {
                    $eq: usuario.senha
                }
            }).toArray(function (err, result) {
                /* Verifico se a consulta retorna algo diferente de vazio. Caso sim, entra no laço */
                if (result[0] != undefined) {

                    /* Crio as variaveis de sessão */
                    req.session.autorizado = true

                    /* Criando as variaveis para usar ao decorrer da aplicação. */
                    req.session.usuario = result[0].usuario
                    req.session.casa = result[0].casa
                }

                if (req.session.autorizado) {
                    res.redirect("jogo")
                    return
                } else {
                    res.render('index', {
                        validacao: {},
                        erroNome: usuario.usuario
                    })
                }
            })

            /* Jeito mais simples, vista que o mesmo recebe um JSON e a condição fica em um JSON dessa mesma forma
                Ex: collection.find(usuario: usuario.usuario, senha: usuario.senha)
            */
            // collection.find(usuario)

            /* Fecho o método open() após a inserção de um registro. */
            mongoclient.close()
        })
    })
}

/**
 * Como todos os arquivos é carregado dentro do consign, eu preciso retornar o mesmo com o modulo.eports passando por retorno a função UsuariosDAO
 * @returns string
 */
module.exports = function () {
    return UsuariosDAO
}