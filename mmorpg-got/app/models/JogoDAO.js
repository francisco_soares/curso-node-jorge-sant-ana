/**
 * Importando o método ObjectId para conseguei transformar string em obj
 */
var ObjectID = require('mongodb').ObjectId

/**
 * O JogoDAO está sendo usado como uma especie de "CLASSE"
 */
function JogoDAO(connection) {
    /* Executo a conexão (connection()) e atribuo essa conexão a variavel _connection. Assim posso usar em outros módulos. */
    this._connection = connection()
}

/**
 * O gerarParametros é como se fosse um método da "CLASSE" JogoDAO
 * 
 * @param {json} usuario 
 */
JogoDAO.prototype.gerarParametros = function (usuario) {
    /**
     * O método open() é utilizando para manupular os DOCUMENTOS/registro no Mongo
     *  1º Parametro: Erro caso retorne.
     *  2º Parametro: O obj cliente com as tratativas.
     */
    this._connection.open(function (err, mongoclient) {

        /** 
         * O collection recebe dois parametros
         *  1º Parametro: É a collection, que no caso, será usuarios
         *  2º Parametro: Função de callback para manipular uma ação.
         */
        mongoclient.collection("jogo", function (err, collection) {
            collection.insert({
                usuario: usuario,
                moeda: 15,
                suditos: 10,
                temor: Math.floor(Math.random() * 1000),
                sabedoria: Math.floor(Math.random() * 1000),
                comercio: Math.floor(Math.random() * 1000),
                magia: Math.floor(Math.random() * 1000)
            })

            /* Fecho o método open() após a inserção de um registro. */
            mongoclient.close()
        })
    })
}

/**
 * O iniciaJogo é como se fosse um método da "CLASSE" JogoDAO
 * 
 * O mesmo é chamado no arquivo mmorpg-got\app\controllers\jogo.js
 * @param {request} res 
 * @param {string} usuario 
 * @param {string} casa 
 * @param {string} comando_invalido 
 */
JogoDAO.prototype.iniciaJogo = function (res, usuario, casa, msg) {
    /**
     * O método open() é utilizando para manupular os DOCUMENTOS/registro no Mongo
     *  1º Parametro: Erro caso retorne.
     *  2º Parametro: O obj cliente com as tratativas.
     */
    this._connection.open(function (err, mongoclient) {

        /** 
         * O collection recebe dois parametros
         *  1º Parametro: É a collection, que no caso, será usuarios
         *  2º Parametro: Função de callback para manipular uma ação.
         */
        mongoclient.collection("jogo", function (err, collection) {

            /* Esse é um jeito de fazer, mais robusto */
            /* O toArray retorna o cursor convertido em array que o find() retorna para o usuário. */
            collection.find({
                usuario: {
                    $eq: usuario
                }
            }).toArray(function (err, result) {

                console.log(result[0])
                /* Passo como parametro no render() a sessão que leva o nome da casa escolhida (img_casa: req.session.casa) */
                res.render('jogo', {
                    img_casa: casa,
                    jogo: result[0], // Como aqui só preciso do resultado do usuário logado, eu passo o result[] na posição 0
                    msg: msg // Aqui é passado a variavel [comando_invalido]. O mesmo é pego no arquivo mmorpg-got\app\views\jogo.ejs
                });
            })

            /* Jeito mais simples, vista que o mesmo recebe um JSON e a condição fica em um JSON dessa mesma forma
                Ex: collection.find(usuario: usuario)
            */
            // collection.find(usuario)

            /* Fecho o método open() após a inserção de um registro. */
            mongoclient.close()
        })
    })
}

/**
 * 
 * @param {json} acao 
 */
JogoDAO.prototype.acao = function (acao) {

    /**
     * O método open() é utilizando para manupular os DOCUMENTOS/registro no Mongo
     *  1º Parametro: Erro caso retorne.
     *  2º Parametro: O obj cliente com as tratativas.
     */
    this._connection.open(function (err, mongoclient) {

        /** 
         * O collection recebe dois parametros
         *  1º Parametro: É a collection, que no caso, será usuarios
         *  2º Parametro: Função de callback para manipular uma ação.
         * 
         * Isso tudo é feito um INSERT no BANCO.
         */
        mongoclient.collection("acao", function (err, collection) {

            // Pego a data em milisegundos
            var date = new Date()

            var tempo = null

            /* 
                Faço o switch para pegar a opção que o usuário selecionar.
                Dessa forma, faço os calculos em milisegundos
             */
            switch (parseInt(acao.acao)) {
                case 1:
                    tempo = 1 * 60 * 6000
                    break;
                case 2:
                    tempo = 2 * 60 * 6000
                    break;
                case 3:
                    tempo = 5 * 60 * 6000
                    break;
                case 4:
                    tempo = 5 * 60 * 6000
                    break;
            }

            /* Aqui faço o calculo para determinar quanto tempo falta para finalizar o trabalho */
            acao.acao_termina_em = date.getTime() + tempo
            collection.insert(acao)


        })

        /**
         * 
         * Fazendo uma operação de UPDATE
         */
        mongoclient.collection("jogo", function (err, collection) {

            var moedas = null
            /**
             * Fazendo tratativas para coleta de mode moedas.
             */
            switch (parseInt(acao.acao)) {
                case 1:
                    moedas = -2 * acao.quantidade
                    break;
                case 2:
                    moedas = -3 * acao.quantidade
                    break;
                case 3:
                    moedas = -1 * acao.quantidade
                    break;
                case 4:
                    moedas = -1 * acao.quantidade
                    break;
            }

            /**
             * Sobre o [INC]
             * - O inc faz o decremento ou incremento quando você passa um valor. Nesse exemplo, ele vai pegar a quantidade de moedas que temos no banco e retirar o valor passado na variavel MOEDAS. Então ficara assim a lógica.
             * 15 - 4 = 11
             */
            collection.update({
                usuario: acao.usuario
            }, {
                $inc: {
                    moeda: moedas
                }
            })

            /* Fecho o método open() após a inserção de um registro. */
            mongoclient.close()

        })
    })
}

JogoDAO.prototype.getAcoes = function (usuario, res) {
    /**
     * O método open() é utilizando para manupular os DOCUMENTOS/registro no Mongo
     *  1º Parametro: Erro caso retorne.
     *  2º Parametro: O obj cliente com as tratativas.
     */
    this._connection.open(function (err, mongoclient) {

        /** 
         * O collection recebe dois parametros
         *  1º Parametro: É a collection, que no caso, será usuarios
         *  2º Parametro: Função de callback para manipular uma ação.
         */
        mongoclient.collection("acao", function (err, collection) {

            // Instancio a classe Date para fazer uma checagem no banco logo abaixo junto ao collection.find()
            var date = new Date()
            var momento_atual = date.getTime()

            /* Esse é um jeito de fazer, mais robusto */
            /* O toArray retorna o cursor convertido em array que o find() retorna para o usuário. */
            collection.find({
                usuario: {
                    $eq: usuario
                },
                acao_termina_em: {
                    $gt: momento_atual
                }
            }).toArray(function (err, result) {

                console.log(result)

                // Estou enviando para dentro da view [pergaminhos] os valores recuperados do result que vem do banco.
                // A variavel acoes será pego no arquivo [mmorpg-got\app\views\pergaminhos.ejs]
                res.render('pergaminhos', {
                    acoes: result
                })


                /* Fecho o método open() após a inserção de um registro. */
                mongoclient.close()
            })

            /* Jeito mais simples, vista que o mesmo recebe um JSON e a condição fica em um JSON dessa mesma forma
                Ex: collection.find(usuario: usuario)
            */
            // collection.find(usuario)


        })
    })
}

JogoDAO.prototype.revogarAcao = function (_id, res) {
    /**
     * O método open() é utilizando para manupular os DOCUMENTOS/registro no Mongo
     *  1º Parametro: Erro caso retorne.
     *  2º Parametro: O obj cliente com as tratativas.
     */
    this._connection.open(function (err, mongoclient) {

        /** 
         * O collection recebe dois parametros
         *  1º Parametro: É a collection, que no caso, será usuarios
         *  2º Parametro: Função de callback para manipular uma ação.
         */
        mongoclient.collection("acao", function (err, collection) {

            /**
             * O ObjectID é uma variavel definida no começo desse documento. Ela serve pra pegar uma variavel e passa-la por parametro para transforma-la em obj
             */
            collection.remove({
                _id: ObjectID(_id)
            }, function (err, result) {
                res.redirect("jogo?msg=D")
                mongoclient.close()
            })
        })
    })
}

/**
 * Como todos os arquivos é carregado dentro do consign, eu preciso retornar o mesmo com o modulo.eports passando por retorno a função JogoDAO
 * @returns string
 */
module.exports = function () {
    return JogoDAO
}