/**
 * Módulo que renderiza a view index.
 * O mesmo é chamado no mmorpg-got\app\routes\index.js
 * 
 * OBS:
 *      - Na função é passado os parametros application, req, res
 * 
 * @param {string} application 
 * @param {string} req 
 * @param {string} res 
 */
module.exports.index = function (application, req, res) {
    /**
     * Renderiza/chama a view index.
     */
    res.render('index', {
        validacao: {},
        erroNome: {}
    });
}

/**
 * Módulo responsável por fazer as autenticação do usuário no jogo.
 * O mesmo é chamado no mmorpg-got\app\routes\index.js
 * 
 * OBS:
 *      - Na função é passado os parametros application, req, res
 * 
 * @param {string} application 
 * @param {string} req 
 * @param {string} res 
 * @returns string de erro
 */
module.exports.autenticar = function (application, req, res) {

    // Pego os dados do formulário aqui.
    var dadosForm = req.body;

    // Faço as validações
    req.assert('usuario', 'Usuário não deve ser vazio.').notEmpty()
    req.assert('senha', 'Senha não deve ser vazio.').notEmpty()

    // Caso na validação acima tenha algum erro, será pego aqui.
    var erros = req.validationErrors()

    // Verifico se há erros.
    if (erros) {

        // Envio para a página de cadastro os campos com erros
        res.render("index", {
            validacao: erros,
            erroNome: {}
        })
        return
    }

    /* Instanciando o método de conexão com o banco */
    var connection = application.config.dbConnection
    /* Método de autenticação via bando de dados */
    var UsuariosDAO = new application.app.models.UsuariosDAO(connection)

    /** Passo por parametro os dados vindo do formulário que está na variavel dadosForm */
    UsuariosDAO.autenticar(dadosForm, req, res)
}