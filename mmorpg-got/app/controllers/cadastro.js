/**
 * Módulo que renderiza a view cadastro.
 * O mesmo é chamado no mmorpg-got\app\routes\cadastro.js
 * 
 * OBS:
 *      - Na função é passado os parametros application, req, res
 * 
 * @param {string} application 
 * @param {string} req 
 * @param {string} res 
 */
module.exports.cadastro = function (application, req, res) {

    /**
     * Renderiza/chama a view cadastro.
     */
    res.render('cadastro', {
        validacao: {},
        dadosForm: {}
    });
}

/**
 * Módulo que renderiza a view cadastrar.
 * O mesmo é chamado no mmorpg-got\app\routes\cadastro.js
 * 
 * @param {string} application 
 * @param {string} req 
 * @param {string} res 
 * @returns 
 */
module.exports.cadastrar = function (application, req, res) {

    // Pego os dados do formulário aqui.
    var dadosForm = req.body;

    // Faço as validações
    req.assert('nome', 'Nome não pode ser vázio').notEmpty()
    req.assert('usuario', 'Usuario não pode ser vázio').notEmpty()
    req.assert('senha', 'Senha não pode ser vázio').notEmpty()
    req.assert('casa', 'Casa não pode ser vázio').notEmpty()

    // Caso na validação acima tenha algum erro, será pego aqui.
    var erros = req.validationErrors()

    // Verifico se há erros.
    if (erros) {

        // Envio para a página de cadastro os campos com erros
        res.render('cadastro', {
            validacao: erros,
            dadosForm: dadosForm
        })
        return
    }

    /** Exportando a conexão com o banco de dados */
    var connection = application.config.dbConnection
    console.log(connection)

    /* Pego a instancia do obj UsuariosDAO que está no arquivo app\models\UsuariosDAO.js */
    var UsuariosDAO = new application.app.models.UsuariosDAO(connection)
    UsuariosDAO.inserirUsuario(dadosForm, req, res)

    /* Geração dos parametros assim que cadastrado o novo usuário. */
    var JogoDAO = new application.app.models.JogoDAO(connection)
    JogoDAO.gerarParametros(dadosForm.usuario)
}

module.exports.sucesso = function (application, req, res) {
    res.render('sucesso', {
        sucesso: {}
    })
}