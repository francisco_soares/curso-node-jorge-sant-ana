/**
 * Módulo que renderiza a view jogo.
 * O mesmo é chamado no mmorpg-got\app\routes\jogo.js
 * 
 * OBS:
 *      - Na função é passado os parametros application, req, res
 * 
 * @param {string} application 
 * @param {string} req 
 * @param {string} res 
 */
module.exports.jogo = function (application, req, res) {

    /**
     * Verifico se o usuário não se logou no jogo
     */
    if (req.session.autorizado !== true) {
        res.send('Usuário precisa fazer login')
        return
    }

    var msg = ''
    if (req.query.msg != '') {
        msg = req.query.msg
    }

    /* Pego o usuário que está sendo vinculado a uma sessão la no arquivo mmorpg-got\app\models\UsuariosDAO.js */
    var usuario = req.session.usuario
    var casa = req.session.casa

    /** Exportando a conexão com o banco de dados */
    var connection = application.config.dbConnection

    /* Envio a conexão com o banco para a "Classe JogoDAO()" */
    var JogoDAO = new application.app.models.JogoDAO(connection)

    /* Inicio o metodo iniciaJogo */
    JogoDAO.iniciaJogo(res, usuario, casa, msg);
}

/**
 * Módulo que renderiza a view index.
 * O mesmo é chamado no mmorpg-got\app\routes\index.js
 * 
 * OBS:
 *      - Na função é passado os parametros application, req, res
 * @param {string} application 
 * @param {string} req 
 * @param {string} res 
 */
module.exports.sair = function (application, req, res) {

    /* Destroindo a sessão e voltando para a Index */
    req.session.destroy(function () {
        res.render('index', {
            validacao: {},
            erroNome: {}
        })
    })

}
/**
 * Módulo que renderiza a view aldeoes.
 * O mesmo é chamado no mmorpg-got\app\routes\jogos.js
 * 
 * OBS:
 *      - Na função é passado os parametros application, req, res
 * @param {string} application 
 * @param {string} req 
 * @param {string} res 
 */
module.exports.suditos = function (application, req, res) {

    /**
     * Verifico se o usuário não se logou no jogo
     */
    if (req.session.autorizado !== true) {
        res.send('Usuário precisa fazer login')
        return
    }

    res.render('aldeoes', {
        validacao: {}
    })

}

/**
 * Módulo que renderiza a view pergaminhos.
 * O mesmo é chamado no mmorpg-got\app\routes\jogos.js
 * 
 * OBS:
 *      - Na função é passado os parametros application, req, res
 * @param {string} application 
 * @param {string} req 
 * @param {string} res 
 */
module.exports.pergaminhos = function (application, req, res) {

    /**
     * Verifico se o usuário não se logou no jogo
     */
    if (req.session.autorizado !== true) {
        res.send('Usuário precisa fazer login')
        return
    }

    /**
     * Recuperar as ações inseridas no banco de dados.
     */
    var connection = application.config.dbConnection
    var JogoDAO = new application.app.models.JogoDAO(connection)

    var usuario = req.session.usuario

    // Executa a função
    JogoDAO.getAcoes(usuario, res)

}

/**
 * Módulo que renderiza a view ordenar_acao_sudito.
 * O mesmo é chamado no mmorpg-got\app\routes\jogos.js
 * 
 * OBS:
 *      - Na função é passado os parametros application, req, res
 * 
 * @param {string} application 
 * @param {string} req 
 * @param {string} res 
 * @returns 
 */
module.exports.ordenar_acao_sudito = function (application, req, res) {

    /**
     * Verifico se o usuário não se logou no jogo
     */
    if (req.session.autorizado !== true) {
        res.send('Usuário precisa fazer login')
        return
    }

    var dadosForm = req.body;

    req.assert('acao', 'Ação deve ser informada').notEmpty()
    req.assert('quantidade', 'Quantidade deve ser informada').notEmpty()

    var erros = req.validationErrors();

    if (erros) {
        res.redirect('jogo?msg=A')
        return
    }

    // Instancio/Pego a conexão com o banco de dados
    var connection = application.config.dbConnection;

    // Instancio a "Classe" JogoDAO()
    var JogoDAO = new application.app.models.JogoDAO(connection)

    // Crio uma nova chave dentro do obj literal, e insiro o usuario da sessão como valor.
    dadosForm.usuario = req.session.usuario
    // Chamo o método acao()
    JogoDAO.acao(dadosForm)

    res.redirect('jogo?msg=B')
}

/**
 * Módulo que renderiza a view revogar_acao.
 * O mesmo é chamado no mmorpg-got\app\routes\jogos.js
 * 
 * OBS:
 *      - Na função é passado os parametros application, req, res
 * 
 * @param {string} application 
 * @param {string} req 
 * @param {string} res 
 */
module.exports.revogar_acao = function (application, req, res) {
    var url_query = req.query


    // Instancio/Pego a conexão com o banco de dados
    var connection = application.config.dbConnection;

    // Instancio a "Classe" JogoDAO()
    var JogoDAO = new application.app.models.JogoDAO(connection)

    var _id = url_query.id_acao
    // Executa a função revogarAcao dentro do obj JogoDAO passando o ID que é recebido na variavel url_query.id_acao. O mesmo vem como GET dentro do arquivo mmorpg-got\app\views\pergaminhos.ejs
    JogoDAO.revogarAcao(_id, res)
}