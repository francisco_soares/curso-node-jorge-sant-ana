/**
 * Módulo que chama as seguintes as views JOGO e SAIR
 * OBS:
 * 		- A view JOGO está sendo chamada via GET
 * 		- A view SAIR está sendo chamada via GET
 *      - Ambas passam os seguintes parametros para suas funções: application, req, res
 *  
 * @param {string} application 
 */
module.exports = function (application) {

	/**
	 * Aqui é acessado o controller jogo() com os parametros passados: application, req, res
	 * 
	 * A controller em questão está no caminho: mmorpg-got\app\controllers\jogo.js
	 */
	application.get('/jogo', function (req, res) {
		application.app.controllers.jogo.jogo(application, req, res)
	});

	/**
	 * Aqui é acessado o controller sair() com os parametros passados: application, req, res
	 * 
	 * A controller em questão está no caminho: mmorpg-got\app\controllers\jogo.js
	 */
	application.get('/sair', function (req, res) {
		application.app.controllers.jogo.sair(application, req, res)
	});

	/**
	 * Aqui é acessado o controller suditos() com os parametros passados: application, req, res
	 * 
	 * A controller em questão está no caminho: mmorpg-got\app\controllers\jogo.js
	 */
	application.get('/suditos', function (req, res) {
		application.app.controllers.jogo.suditos(application, req, res)
	});

	/**
	 * Aqui é acessado o controller pergaminhos() com os parametros passados: application, req, res
	 * 
	 * A controller em questão está no caminho: mmorpg-got\app\controllers\jogo.js
	 */
	application.get('/pergaminhos', function (req, res) {
		application.app.controllers.jogo.pergaminhos(application, req, res)
	});

	/**
	 * Aqui é acessado o controller ordenar_acao_sudito() com os parametros passados: application, req, res
	 * 
	 * A controller em questão está no caminho: mmorpg-got\app\controllers\jogo.js
	 */
	application.post('/ordenar_acao_sudito', function (req, res) {
		application.app.controllers.jogo.ordenar_acao_sudito(application, req, res)
	});

	/**
	 * Aqui é acessado o controller revogar_acao() com os parametros passados: application, req, res
	 * 
	 * A controller em questão está no caminho: mmorpg-got\app\controllers\jogo.js
	 */
	application.get('/revogar_acao', function (req, res) {
		application.app.controllers.jogo.revogar_acao(application, req, res)
	});
}