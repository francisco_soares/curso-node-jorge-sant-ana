/**
 * Módulo que chama as seguintes as views CADASTRO e CADASTRAR
 * OBS:
 * 		- A view CADASTRO está sendo chamada via GET
 * 		- A view CADASTRAR está sendo chamada via POST
 *      - Ambas passam os seguintes parametros para suas funções: application, req, res
 * @param {string} application 
 */
module.exports = function (application) {
	application.get('/cadastro', function (req, res) {
		application.app.controllers.cadastro.cadastro(application, req, res)
	});

	// Rota que recebe as informações enviadas pelo formulário de cadastro
	application.post('/cadastrar', function (req, res) {
		application.app.controllers.cadastro.cadastrar(application, req, res)
	});

	// Rota que recebe as informações enviadas pelo formulário de cadastro
	application.get('/sucesso', function (req, res) {
		application.app.controllers.cadastro.sucesso(application, req, res)
	});

	// Rota que recebe as informações enviadas pelo formulário de cadastro
	application.post('/sucesso', function (req, res) {
		application.app.controllers.cadastro.sucesso(application, req, res)
	});
}