/**
 * Módulo que chama as seguintes as views INDEX e AUTENTICAR
 * OBS:
 * 		- A view INDEX está sendo chamada via GET
 * 		- A view AUTENTICAR está sendo chamada via POST
 *      - Ambas passam os seguintes parametros para suas funções: application, req, res
 * 
 * @param {string} application 
 */
module.exports = function (application) {

	/**
	 * Aqui é acessado o controller index() com os parametros passados: application, req, res
	 * 
	 * A controller em questão está no caminho: mmorpg-got\app\controllers\index.js
	 */
	application.get('/', function (req, res) {
		application.app.controllers.index.index(application, req, res)
	});

	/**
	 * Aqui é acessado o controller autenticar() com os parametros passados: application, req, res
	 * 
	 * A controller em questão está no caminho: mmorpg-got\app\controllers\index.js
	 * 
	 * Rota que recebe as informações enviadas pelo formulário de login
	 */
	application.post('/autenticar', function (req, res) {
		application.app.controllers.index.autenticar(application, req, res)
	});
}