/** Para executar esse console pelo NODE, você deve seguir os passos abaixo. 
 * 
 * Acesse a pasta do projeto
 * cd curso-node-jorge-sant-ana
 * 
 * Execute o comando abaixo
 * node <nome-arquivo.js>
 * node aula-7-executando-arquivos-javascript-no-node.js
 * 
 * ATENÇÃO: 
 *  - Você precisa estar dentro da pasta onde o arquivo será executado
 *  - Você pode usar o bash para fazer esse comando. Não precisa necessariamente do prompt de comando.
*/
console.log("Criando um site de notícias em NodeJS")